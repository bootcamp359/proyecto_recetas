const express = require('express');
const mongoose=require("mongoose")
const userSchema=require('./models/user')
const recipeSchema=require('./models/recipe');
const app = express();


mongoose.connect('mongodb://luis:luis@mongorecetas:27017/miapp?authSource=admin')
.then(() => console.log('conectado a Mongo'))
.catch((error) => console.error(error))
//Middleware
app.use(express.json())

// Routes
app.post('/new_user', (req,res) => {
    const user = userSchema(req.body);
    user.save()
    .then((data) => res.json(data))
    .catch((error) => res.send(error))
})


app.post('/new_recipe', (req,res) => {
    const recipe = recipeSchema(req.body);
    recipe.save()
    .then((data) => res.json(data))
    .catch((error) => res.send(error))
})

app.post('/rate', (req,res) => {
    const {recipeId, userId, rating}=req.body
    recipeSchema.updateOne(
        {_id:recipeId},
        [
            { $set: { ratings: {$concatArrays: [{$ifNull: ['$ratings', []]}, [{ userId: userId, rating: rating}]]}} },
            {$set: {avgRating: {$trunc: [{$avg:['$ratings.rating']},0]}}}
        ]
    ).then((data)=> res.json(data))
    .catch((error) => res.send(error))
})

app.get('/recipes', (req,res) => {
    const {userId,recipeId}=req.body
    if(recipeId){9
        recipeSchema
        .find({_id:recipeId})
        .then((data) => res.json(data))
        .catch((error) => res.send(error))
    }else{
        recipeSchema
        .find({userId:userId})
        .then((data) => res.json(data))
        .catch((error) => res.send(error))
    }
})

app.get('/recipesbyingredient',(req,res)=>{
const{ingredients}=req.body
const ingredientsArray=ingredients.map(a=>a.name)
recipeSchema
.find({ingredients:{$elemMatch:{name:{$in:ingredientsArray}}}})
.then((data) => res.json(data))
.catch((error) => res.send(error))

})

app.listen(3000, () => console.log("Esperando en puerto 3000..."))
